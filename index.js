const { receiveFile } = require('./src/InputReceiver.js'); 
const { optimize } = require('./src/Optimizer.js');
const { writeFile } = require('./src/OutputWriter.js');



const {inFileName, offset, inSchedule} = receiveFile();

const optimizedSchedule = optimize(inSchedule);

writeFile(optimizedSchedule, inFileName, offset);