
const getFirstPerformance = (inSchedule) => {
    return inSchedule.reduce(function(prev, current) {
        if (current.start < prev.start) {
            return current;
        } else if (current.start === prev.start) {
            if (current.priority > prev.priority) {
                return current;
            }
        }
        return prev;
    });
};

const getEndPerformance = (inSchedule) => {
    return inSchedule.reduce(function(prev, current) {
        if (prev.finish > current.finish) {
            return prev;
        } else if (prev.finish === current.finish) {
            if (prev.priority > current.priority) {
                return prev;
            }
        }
        return current;
    });
}

const getNextPerformance = (inSchedule, currentPerformance) => {
    const filteredScedule = inSchedule.filter(performance => {
        if (performance.band !== currentPerformance.band) {
            if (performance.finish > currentPerformance.start){
                if (performance.priority > currentPerformance.priority || performance.finish > currentPerformance.finish) {
                    if (performance.priority < currentPerformance.priority && performance.start < currentPerformance.finish) {
                        performance.start = currentPerformance.finish;
                    }
                    return performance;
                }
            }
        }
    });

    if (filteredScedule.length) {
        const nextInLine = getFirstPerformance(filteredScedule, currentPerformance.start);
        if (nextInLine.priority <= currentPerformance.priority && nextInLine.start < currentPerformance.finish) {
            nextInLine.start = currentPerformance.finish;
        }
        return nextInLine;
    }
    return null;
}

exports.getNextPerformance  = getNextPerformance;
exports.getEndPerformance   = getEndPerformance;
exports.getFirstPerformance = getFirstPerformance;