const fs = require('fs');

const receiveFile = () => {
    const myArgs = process.argv.slice(2);
    const inFileName = myArgs[0];

    const rawdata = fs.readFileSync(inFileName);
    const rawSchedule = JSON.parse(rawdata);
    const offset = rawSchedule[0].start.includes('+') ? rawSchedule[0].start.split('+').pop() : null;

    const inSchedule = rawSchedule.map((performance) => {
        performance.start = new Date(offset ? performance.start.split('+')[0] : performance.start).getTime();
        performance.finish = new Date(offset ? performance.finish.split('+')[0] :performance.finish).getTime();
        return performance;
    });
    return {inFileName, offset, inSchedule};
};

exports.receiveFile = receiveFile;