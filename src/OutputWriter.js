const fs = require('fs');

const writeFile = (optimizedSchedule, inFileName, offset) => {
    const outSchedule = optimizedSchedule.map((performance) => {
        if (offset) {
            return {
                'band' : performance.band,
                'start' : new Date(performance.start).toISOString().split('.')[0]+"+"+offset,
                'finish' : new Date(performance.finish).toISOString().split('.')[0]+"+"+offset,
            }
        }
        return {
            'band' : performance.band,
            'start' : new Date(performance.start).toISOString().split('.')[0]+"Z",
            'finish' : new Date(performance.finish).toISOString().split('.')[0]+"Z",
        }
    });

    const stringSchedule = JSON.stringify(outSchedule, null, 2);
    fs.writeFileSync(inFileName.replace(/\.[^/.]+$/, '') + '.optimal.json', stringSchedule);
};

exports.writeFile = writeFile;