const { getFirstPerformance, getEndPerformance, getNextPerformance } = require('./Selector.js');

const optimize = (inSchedule) => {
    let optimizedSchedule = [];
    const firstPerformance = getFirstPerformance(inSchedule);
    
    const lastPerformance = getEndPerformance(inSchedule);
    
    optimizedSchedule.push(Object.assign({}, firstPerformance));
    let optimisedStart = firstPerformance.start;

    while(optimisedStart < lastPerformance.finish) {
        const currentPerformance = optimizedSchedule[optimizedSchedule.length - 1];
        const nextPerformance = getNextPerformance(inSchedule, currentPerformance);
        
        if (nextPerformance) {
            if (nextPerformance.start < currentPerformance.finish) {
                currentPerformance.finish = nextPerformance.start;
            }

            optimizedSchedule.push(nextPerformance);
            optimisedStart = nextPerformance.start;
        } else {
            break;
        }
    }
    return optimizedSchedule;
};

exports.optimize = optimize;