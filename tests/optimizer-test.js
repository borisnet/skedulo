const assert = require('assert')
const { optimize } = require('../src/Optimizer.js');

const inSchedule = [ 
    {
        band: 'Soundgarden',
        start: 738295200000,
        finish: 738298200000,
        priority: 5 
    },
    { 
        band: 'Pearl Jam',
        start: 738296100000,
        finish: 738297300000,
        priority: 9 
    } 
];

const expectedOptimalSchedule = [ 
    { 
        band: 'Soundgarden',
        start: 738295200000,
        finish: 738296100000,
        priority: 5 
    },
    { 
        band: 'Pearl Jam',
        start: 738296100000,
        finish: 738297300000,
        priority: 9 
    },
    { 
        band: 'Soundgarden',
        start: 738297300000,
        finish: 738298200000,
        priority: 5 
    } 
];

test('should return optimized schedule', () => {
    assert.deepEqual(optimize(inSchedule), expectedOptimalSchedule);
});