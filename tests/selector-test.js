const assert = require('assert')
const { getFirstPerformance, getEndPerformance, getNextPerformance } = require('../src/Selector.js');

const testSchedule = [
    {
      "band" : "Soundgarden",
      "start" : "1993-05-25T02:00:00Z",
      "finish" : "1993-05-25T02:50:00Z",
      "priority" : 5
    },
    {
      "band" : "Pearl Jam",
      "start" : "1993-05-25T02:15:00Z",
      "finish" : "1993-05-25T02:35:00Z",
      "priority" : 9
    }
];

const firstPerformance = {
    "band" : "Soundgarden",
    "start" : "1993-05-25T02:00:00Z",
    "finish" : "1993-05-25T02:50:00Z",
    "priority" : 5
};

const nextPerformance = {
    "band" : "Pearl Jam",
      "start" : "1993-05-25T02:15:00Z",
      "finish" : "1993-05-25T02:35:00Z",
      "priority" : 9
};

const lastPerformance = {
    "band" : "Soundgarden",
    "start" : "1993-05-25T02:00:00Z",
    "finish" : "1993-05-25T02:50:00Z",
    "priority" : 5
};

test('should return first performance', () => {
    assert.deepEqual(getFirstPerformance(testSchedule), firstPerformance);
});

test('should return next performance', () => {
    assert.deepEqual(getNextPerformance(testSchedule, lastPerformance), nextPerformance);
});

test('should return last performance', () => {
    assert.deepEqual(getEndPerformance(testSchedule), lastPerformance);
});

