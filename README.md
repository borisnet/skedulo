# Programming Challenge 

## Background 
This is a pure backend JavaScript Node based solution, only assumption about environment is that Node is installed. Run and tested against Docker Ubuntu container with Node 10. There are no packages to install, no node modules, and nothing to compile or build.

### All commands assumed executed from project root directory

**How to run**

```
Make sure file executable: chmod +x verify-music.sh
To Verify:                ./verify-music.sh index.js 
Run single file:         node index.js input/example.json
Run unit tests:           node test.js ./tests/*-test.js
```

### There is bare minimal unit tests coverage
Tests are using default Node assertions again no testing package to install.

### Limitations:

1. Turns out pure vanilla JavaScript is extremely limited in handling dates. Normally I would have a package like Moment JS parse dates and handle timezones, sticking to pure JS proved a challendge. There is a limitation with offset format - it will only work with positive + hours and will fail on negative - hours offset. Cutting corners to save time and focus on the important parts.

2. There probably are millions of other schedule cases and combinations that will make this fail miserably. As this is a test with a limited time allocation I didn't go futher, but normally in the testing I would be looking to cover edge cases beyond whats already covered in verify - all possible combinations of priority and date overlapping scenarious, different timezones (if program was capable to handle timezones). 

3. Nice to have - random generated tests that throw 1000's of crazy dates and priorities combinations at the solution.

4. Nice to have - data verfication and edge case handling like start date after end date.